const express = require('express')
const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy;
const credentials = require('../../credentials/facebook.json')

const port = 5000

async function authenticate() {
    const webServer = _startWebServer()
    const accessToken = await _waitForFacebookCallback(webServer.app)
    await _stopWebServer(webServer)
    return accessToken;
}

function _startWebServer() {
    const app = express()

    const server = app.listen(port)
    server.setTimeout(3000)
    return {
        app,
        server
    }
}

async function _stopWebServer(webServer) {
    return new Promise((resolve, reject) => {
        webServer.server.close(() => {
            resolve()
        })
    });
}

function _configurePassportFacebook(callbackFn) {
    passport.use(new FacebookStrategy({
        clientID: credentials.client_id,
        clientSecret: credentials.client_secret,
        callbackURL: `http://localhost:${port}/auth/facebook/callback`,
    },
        callbackFn
    ))
}

async function _waitForFacebookCallback(app) {
    return new Promise((resolve, reject) => {

        const onAuthenticate = (accessToken, refreshToken, profile, done) => {
            const { displayName } = profile
            console.log(`> [facebook-auth] ${displayName} autenticado com sucesso!`)
            resolve(accessToken);
            return done(null, profile)
        }

        _configurePassportFacebook(onAuthenticate)

        app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['publish_to_groups'] }))
        console.log(`> [facebook-auth] Por favor se autentique em http://localhost:${port}/auth/facebook`)

        app.get('/auth/facebook/callback', passport.authenticate('facebook', { session: false }), (req, res) => {
            res.send(`<b>Autenticado com sucesso!</b> <br><br> Você já pode fechar esta aba.`)
        })
    })
}


module.exports = {
    authenticate
}