const facebook = require('./components/facebook')

initApplication()

async function initApplication() {
    const acessToken = await facebook.authenticate();
    console.log('> Token de acesso: ' + acessToken)
}